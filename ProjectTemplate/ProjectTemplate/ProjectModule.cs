﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using System.ComponentModel.Composition;
using Ipsumm.IRIS.Infrastructure.Modularity;
using Ipsumm.IRIS.Infrastructure;
using Ipsumm.IRIS.Infrastructure.Devices;
using Ipsumm.IRIS.Infrastructure.Devices.Events;
using Ipsumm.IRIS.Infrastructure.Services;

namespace Ipsumm.CustomAutomation.Project
{
    [ModuleExport(typeof(ProjectModule), InitializationMode = InitializationMode.WhenAvailable)]
    public class ProjectModule : ModuleBase
    {
        [ImportingConstructor]
        public ProjectModule(ICommonServices services) : base(services) { }

        /// <summary>
        /// Initializes the current module.
        /// </summary>
        public override void Initialize()
        {
            Services.Log.Initializing("Project Module");

            RegisterUIComponents();
        }

        private void RegisterUIComponents()
        {
            var rm = Services.RegionManager;


        }

    }
}
