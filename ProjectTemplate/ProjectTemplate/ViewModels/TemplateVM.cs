﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ipsumm.IRIS.Infrastructure.MVVM;
using Ipsumm.IRIS.Infrastructure.Services;
using Prism.Commands;

namespace Ipsumm.CustomAutomation.Project.ViewModels
{
    [Export]
    public class TemplateVM : ViewModelBase
    {
        [ImportingConstructor]
        public TemplateVM(ICommonServices services) : base(services)
        {
            InitializeCommands();
        }

        public TemplateCommands Commands { get; private set; }

        private void InitializeCommands()
        {
            Commands = new TemplateCommands();

            Commands.Command1 = new DelegateCommand(() =>
            {
                    //do something 
            });
        }

    }
}
